import { ALL, cardWidth, SMALL_SCREEN_SIZE, CardMode, translateType } from './../../assets/constants';
import { DataService, ClientShortData, ClientData, CollectionData } from './../data.service';
import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-clientsPage',
  templateUrl: './clientsPage.component.html',
  styleUrls: ['./clientsPage.component.css']
})
export class ClientsPageComponent {
  smallScreenWidth = false;
  data: ClientShortData[] | null = null;

  displayedColumns: string[] = [
    'name', 'inn', 'date', 'clientTypeString', 'clientClassificationString'
  ];
  columnsOptions = {
    'name': { type: 'text', title: 'Название' },
    'inn': { type: 'text', title: 'ИНН' },
    'date': { type: 'date', title: 'Дата регистрации' },
    'clientTypeString': { type: 'select', title: 'Тип', options: [] },
    'clientClassificationString': { type: 'select', title: 'Классификация', options: [] },
  }
  filters = {
    'name': null,
    'inn': null,
    'date': null,
    'clientTypeString': null,
    'clientClassificationString': null,
  }
  filterValue = '';

  cardOpened = false;
  cardMode: CardMode = CardMode.VIEW;
  collectionsList: CollectionData[] | null = null;
  currentItem: ClientData | null = null;
  innChangeError: string | null = null;
  itemFields = {
    'inn': { type: 'text', editable: false, title: 'ИНН', required: true },
    'name': { type: 'text', editable: false, title: 'Название', required: true },
    'date': { type: 'date', editable: false, title: 'Дата регистрации', required: false },
    'email': { type: 'text', editable: true, title: 'E-mail', required: false },
    'phone': { type: 'text', editable: true, title: 'Телефон', required: false },
    'address': { type: 'text', editable: true, title: 'Адрес', required: false },
    'site': { type: 'text', editable: true, title: 'Сайт', required: false },
    'clientTypeString': {
      type: 'select',
      editable: true,
      title: 'Тип',
      options: [],
      required: true
    },
    'clientClassificationString': {
      type: 'select',
      editable: true,
      title: 'Классификация',
      options: [],
      required: true,
    },
    'salesVolume': { type: 'table', editable: true, title: 'Дистрибьюторы', required: false }
  };

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.smallScreenWidth = event.target.innerWidth <= SMALL_SCREEN_SIZE;
  }

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.smallScreenWidth = window.innerWidth < SMALL_SCREEN_SIZE;
    this.dataService.getClientList().subscribe(
      res => {
        // console.log(res);
        this.data = res;
      },
      err => {
        console.log(err);
        this.data = null;
      }
    );
    const types = this.dataService.getTypes();
    this.columnsOptions.clientTypeString.options = [ALL, ...types];
    this.itemFields.clientTypeString.options = types;

    this.dataService.getClassifications().subscribe(
      data => {
        const classifications = data.map(item => item.name);
        this.columnsOptions.clientClassificationString.options = [ALL, ...classifications];
        this.itemFields.clientClassificationString.options = classifications;
      },
      err => {
        console.log(err);
        this.columnsOptions.clientClassificationString.options = [];
        this.itemFields.clientClassificationString.options = [];
      });

      this.dataService.getCollections().subscribe(
        res => {
          this.collectionsList = res;
        },
        err => {
          console.log(err);
          this.collectionsList = null;
        }
      )
  }

  onRowClick(event) {
    this.cardOpened = true;
    this.getClientData(event.id);
    this.cardMode = CardMode.VIEW;
  }

  newItem() {
    this.cardOpened = true;
    this.currentItem = {};
    this.cardMode = CardMode.CREATE;
  }

  closeCard() {
    this.cardOpened = false;
    this.currentItem = null;
    this.cardMode = CardMode.VIEW;
  }

  enableEditMode() {
    this.cardMode = CardMode.EDIT;
  }

  onLeftButtonClick(itemData) {
    if (this.cardMode === CardMode.VIEW) {
      this.cardMode = CardMode.EDIT;
    } else {
      if (this.cardMode === CardMode.CREATE) {
        this.dataService.saveNewClient(this.currentItem);
      } else {
        this.dataService.saveClient(this.currentItem);
      }
      // save, if success => VIEW
      this.cardMode = CardMode.VIEW;
    }
  }

  onRightButtonClick() {
    if (this.cardMode === CardMode.EDIT) {
      this.cardMode = CardMode.VIEW;
      // discard
      this.getClientData(this.currentItem.id);
    } else {
      // VIEW OR CREATE
      this.closeCard();
    }
  }

  onInnChange(inn) {
    if (inn) {
      // find
      this.dataService.searchClientByInn(inn).subscribe(
        res => {
          this.innChangeError = 'Клиент с таким ИНН уже существует';
        },
        err => {
          this.dataService.searchByInn(inn).subscribe(
            res => {
              console.log(res);
              if (Array.isArray(res)) {
                console.log('[]')
                this.innChangeError = 'ИНН не найден';
              }
              this.innChangeError = null;
              this.currentItem = res;
            },
            err => {
              console.log(err);
              this.innChangeError = 'ИНН не найден';
            });
        }
      )
    } else {
      this.innChangeError = null;
    }
  }

  getClientData(id: number) {
    this.dataService.getClientById(id).subscribe(
      res => {
        // console.log(res);
        this.currentItem = res;
      },
      err => {
        console.log(err);
        this.currentItem = null;
      }
    )
  }

  changeFilters(event) {
    let { column, value } = event;
    if (value === '') {
      value = null;
    }
    this.filters[column] = value;
    this.filterValue = this.filters[column];
  }

  getFilterValue() {
    return this.filters;
  }

  getCollections() {
    if (this.currentItem && this.currentItem.clientTypeString) {
      let type = translateType[this.currentItem.clientTypeString];
      // console.log(this.currentItem.clientTypeString);
      // console.log(translateType);
      
      const data = this.collectionsList.map(item => ({
        name: item.name,
        coef: item[type]
      }));
      const columnsOptions = {
        'name': { type: 'text', title: 'Коллекция'},
        'coef': { type: 'number', title: 'Коэффициент'},
      }
      return {data, columnsOptions};
    }
    return null;
  }

  getCardStyle() {
    let style = {
      // 'height': '90vh',
      'position': 'absolute',
      'top': '10px',
      'bottom': '10px',
      'right': '10px',
      'width': `${this.cardOpened ? cardWidth : 0}px`,
      'background-color': 'white',
    }
    if (this.smallScreenWidth) {
      style['left'] = '10px';
      style['width'] = `calc(100% - 20px)`;
    }
    return style;
  }

  getStyle() {
    return {
      'width': `calc(100% - ${this.cardOpened ? cardWidth + 10 : 0}px)`,
    }
  }

  shouldShowTable() {
    return !(this.cardOpened && this.smallScreenWidth);
  }

  getButtonsStyle() {
    if (this.smallScreenWidth) {
      return {
        'position': 'absolute',
        'right': '10px',
        'bottom': '10px',
      };
    } else {
      return {
        'position': 'absolute',
        'right': '10px',
        'top': '10px',
      };
    }
  }
}
