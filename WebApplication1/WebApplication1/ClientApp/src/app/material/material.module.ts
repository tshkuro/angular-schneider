import { MatButtonModule, MatInputModule, MatGridListModule, MatSidenavModule, MatListModule, MatMenuModule, MatDividerModule, MatSortModule, MatPaginatorModule, MatSelectModule, MatDatepickerModule, MatFormFieldModule, MatNativeDateModule, MatProgressSpinnerModule, MatAutocompleteModule } from '@angular/material';
import {MatIconModule} from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { NgModule } from '@angular/core';
import { MaterialComponent } from './material.component';

const MaterialComponents = [
  MatButtonModule,
  MatIconModule,
  MatInputModule,
  MatGridListModule,
  MatSidenavModule,
  MatListModule,
  MatMenuModule,
  MatDividerModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatSelectModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatGridListModule,
  MatProgressSpinnerModule,
  MatAutocompleteModule,
]

@NgModule({
  imports: [MaterialComponents],
  exports: [MaterialComponents],
  declarations: [MaterialComponent]
})
export class MaterialModule { }
