import { Component, Input } from '@angular/core';

@Component({
    selector: 'my-tab',
    styles: [
        `
        .pane{
            padding: 1em;
            border: 1px solid lightgrey;
            border-top: none;
            border-radius: 0 0 10px 10px;
        }
        `
    ],
    template:  `
    <div [hidden]="!active" class="pane">
        <ng-content></ng-content>
    </div>
    `
})
export class TabComponent {
    @Input('tabTitle') title: string;
    @Input() active = false;
}