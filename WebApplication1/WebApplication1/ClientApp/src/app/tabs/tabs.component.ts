import { TabComponent } from './tab.component';
import { Component,
    ContentChildren,
    QueryList,
    AfterContentInit,
    // ViewChild,
    // ComponentFactoryResolver,
    // ViewContainerRef 
} from "@angular/core";

@Component({
    selector: 'my-tabs',
    template: `
        <ul class="nav nav-tabs my-tabs">
            <li *ngFor="let tab of tabs" (click)="selectTab(tab)" [class.active]="tab.active">
                <div class="tab-item" [class.active]="tab.active">{{tab.title}}</div>
            </li>
        </ul>
        <ng-content></ng-content>
    `,
    styles: [
        `
        .my-tabs {
            justify-content: center;
        }
        .tab-close {
            color: gray;
            text-align: right;
            cursor: pointer;
        }

        .tab-item {
            cursor: pointer;
            border: solid 1px lightgrey;
            border-radius: 5px 5px 0px 0px;
            margin-right: 2px;
            padding: 5px 20px 5px 20px;
        }

        .active {
            color: steelblue;
            font-weight: bold;
        }
        `
    ]
})
export class TabsComponent implements AfterContentInit {
    @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;

    ngAfterContentInit() {
        // debugger
        let activeTabs = this.tabs.filter(tab => tab.active);
        if(activeTabs.length === 0) {
            this.selectTab(this.tabs.first);
        }
    }

    selectTab(tab: TabComponent) {
        this.tabs.toArray().forEach(tab =>
            tab.active = false);
        tab.active = true;
    }
}