import { Router, ActivatedRoute } from '@angular/router';
import { DataService, UserData } from './../data.service';
import { CardMode, SMALL_SCREEN_SIZE, cardWidth, paths } from './../../assets/constants';
import { Component, OnInit, HostListener } from '@angular/core';
import { DistributorShortData, DistributorData } from '../data.service';

interface CardTableData {
  data: Object[];
  columnsOptions: Object;
}

@Component({
  selector: 'app-distributorsPage',
  templateUrl: './distributorsPage.component.html',
  styleUrls: ['./distributorsPage.component.css']
})
export class DistributorsPageComponent implements OnInit {
  smallScreenWidth = false;
  data: DistributorShortData[] | null = null;
  displayedColumns: string[] = [
    'name', 'inn', 'date', 'email', 'mobilePhone', 'phone', 'contactPerson', 'address'
  ];
  columnsOptions = {
    'name': { type: 'text', title: 'Название' },
    'inn': { type: 'text', title: 'ИНН' },
    'date': { type: 'date', title: 'Дата регистрации' },
    'email': { type: 'text', title: 'E-mail' },
    'mobilePhone': { type: 'text', title: 'Мобильный телефон' },
    'phone': { type: 'text', title: 'Телефон' },
    'contactPerson': { type: 'text', title: 'Контактное лицо' },
    'address': { type: 'text', title: 'Адрес' },
  }
  displayedColumnsForFilters: string[] = [
    'name', 'inn', 'date', 'email'
  ]

  filters = {
    'name': null,
    'inn': null,
    'date': null,
  }
  filterValue = '';

  cardOpened = false;
  cardMode: CardMode = CardMode.VIEW;
  currentId: number | null = null;
  currentItem: DistributorData | null = null;
  innChangeError: string | null = null;
  itemFields = {
    'inn': { type: 'text', editable: false, title: 'ИНН', required: true },
    'name': { type: 'text', editable: false, title: 'Название', required: true },
    'phone': { type: 'text', editable: true, title: 'Телефон', required: false },
    'email': { type: 'text', editable: true, title: 'E-mail', required: false },
    'address': { type: 'text', editable: true, title: 'Адрес', required: false },
    'mobilePhone': { type: 'text', editable: true, title: 'Мобильный телефон', required: false },
    'contact': { type: 'text', editable: true, title: 'Сайт', required: false },
    'date': { type: 'date', editable: false, title: 'Дата регистрации', required: false },
    'description': { type: 'date', editable: false, title: 'Дополнительная информация', required: false },
  };

  usersList: CardTableData | null = null;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.smallScreenWidth = event.target.innerWidth <= SMALL_SCREEN_SIZE;
  }
  constructor(
    private dataService: DataService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      res => {
        if (res.id) {
          this.currentId = res.id;
          this.openItem(res.id);
        }
      },
      err => { console.log(err); }
    )
    
    this.smallScreenWidth = window.innerWidth < SMALL_SCREEN_SIZE;
    this.dataService.getDistributorList().subscribe(
      res => {
        this.data = res;
      },
      err => {
        console.log(err);
        this.data = null;
      }
    );
  }

  onRowClick(event) {
    if (this.currentId) {
      this.currentId = null;
      this.router.navigate([paths.distributors]);
    }
    this.openItem(event.id);
  }

  openItem(id: number) {
    this.cardOpened = true;
    this.getDistributorData(id);
    this.getDistributorUsers(id);
    this.cardMode = CardMode.VIEW;
  }

  newItem() {
    this.cardOpened = true;
    this.currentItem = {};
    this.cardMode = CardMode.CREATE;
  }

  closeCard() {
    this.cardOpened = false;
    this.currentItem = null;
    this.cardMode = CardMode.VIEW;
    if (this.currentId) {
      this.currentId = null;
      this.router.navigate([paths.distributors]);
    }
  }

  enableEditMode() {
    this.cardMode = CardMode.EDIT;
  }

  onLeftButtonClick(itemData) {
    if (this.cardMode === CardMode.VIEW) {
      this.cardMode = CardMode.EDIT;
    } else {
      // save, if success => VIEW
      if (this.cardMode === CardMode.EDIT) {
        this.dataService.saveDistributor(this.currentItem);
      } else {
        this.dataService.saveNewDistributor(this.currentItem);
      }
      this.cardMode = CardMode.VIEW;
    }
  }

  onRightButtonClick() {
    if (this.cardMode === CardMode.EDIT) {
      this.cardMode = CardMode.VIEW;
      // discard
      this.getDistributorData(this.currentItem.id);
    } else {
      // VIEW OR CREATE
      this.closeCard();
    }
  }

  getDistributorData(id: number) {
    this.dataService.getDistributorById(id).subscribe(
      res => {
        this.currentItem = res;
      },
      err => {
        console.log(err);
        this.currentItem = null;
      }
    )
  }

  getDistributorUsers(id: number) {
    this.dataService.getDistributorUsers(id).subscribe(
      res => {
        if (res.length > 0) {
          this.usersList = {
            data: res,
            columnsOptions: {
              'username': { type: 'text', title: 'E-mail' },
              'fullName': { type: 'text', title: 'ФИО' },
            }
          };
        } else {
          this.usersList = null;
        }
      },
      err => {
        console.log(err);
        this.usersList = null;
      }
    )
  }

  changeFilters(event) {
    let { column, value } = event;
    if (value === '') {
      value = null;
    }
    this.filters[column] = value;
    this.filterValue = this.filters[column];
  }

  getFilterValue() {
    return this.filters;
  }

  getCardStyle() {
    let style = {
      // 'height': '90vh',
      'position': 'absolute',
      'top': '10px',
      'bottom': '10px',
      'right': '10px',
      'width': `${this.cardOpened ? cardWidth : 0}px`,
      'background-color': 'white',
    }
    if (this.smallScreenWidth) {
      style['left'] = '10px';
      style['width'] = `calc(100% - 20px)`;
    }
    return style;
  }

  getStyle() {
    return {
      'width': `calc(100% - ${this.cardOpened ? cardWidth + 10 : 0}px)`,
    }
  }

  shouldShowTable() {
    return !(this.cardOpened && this.smallScreenWidth);
  }

  getButtonsStyle() {
    if (this.smallScreenWidth) {
      return {
        'position': 'absolute',
        'right': '10px',
        'bottom': '10px',
      };
    } else {
      return {
        'position': 'absolute',
        'right': '10px',
        'top': '10px',
      };
    }
  }

  onInnChange(inn) {
    if (inn) {
      this.dataService.searchDistributorByInn(inn).subscribe(
        res => {
          this.innChangeError = 'Дистрибютор с таким ИНН уже существует';
        },
        err => {
          this.dataService.searchByInn(inn).subscribe(
            res => {
              if (Array.isArray(res)) {
                this.innChangeError = 'ИНН не найден';
              }
              this.innChangeError = null;
              this.currentItem = res;
            },
            err => {
              console.log(err);
              this.innChangeError = 'ИНН не найден';
            });
        }
      )
    } else {
      this.innChangeError = null;
    }
  }

}
