import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL } from '../assets/constants';
import jwt from 'jwt-decode';

interface AuthResponse {
  status?: number;
  token_type: string;
  access_token: string;
}

interface UserData {
  sub: number;
  username: string;
  roleName: string;
  fullName: string;
  distributorId: number;
  distributorName: string;
  iat: number;
  exp: number;
  token?: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token: string | null = null;
  userData: UserData | null = null;
  constructor(private http: HttpClient) { }

  get isLoggedIn() {
    this.userData = JSON.parse(localStorage.getItem("schneider_user"));
    return this.userData !== null && this.userData !== undefined;
  }

  setToken(token: string | null) {
    if (token) {
      this.userData = jwt(token);
      this.userData.token = token;
      localStorage.setItem("schneider_user", JSON.stringify(this.userData));
      const exp = this.userData.exp;
      const iat = this.userData.iat;
      setTimeout(() => this.logout(), (exp - iat) * 1000);
    } else {
      localStorage.removeItem("schneider_user");
    }
  }

  login(username, password) {
    const url = URL + '/auth/signin';
    const body = JSON.stringify({ username, password });
    const headers = { 'Content-Type': 'application/json' };
    return this.http.post<AuthResponse>(url, body, { headers });
  }

  logout() {
    localStorage.removeItem('schneider_user');
    this.token = null;
  }

  authHeader() {
    const userData = JSON.parse(localStorage.getItem("schneider_user"));
    if (userData.token) {
      return {
        Authorization: `Bearer ${userData.token}`
      };
    } else {
      return {};
    }
  }
}

