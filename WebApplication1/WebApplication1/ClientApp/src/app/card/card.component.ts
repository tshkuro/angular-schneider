import { CardMode, phoneRegex, innRegex } from './../../assets/constants';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input() cardMode;
  @Input() currentItem;
  @Input() itemFields;
  @Input() cardTitle;
  @Input() innChangeError;
  @Input() collectionsList;
  @Input() usersList;
  @Output() leftBtnEvent = new EventEmitter();
  @Output() rightBtnEvent = new EventEmitter();
  @Output() innChangeEvent = new EventEmitter();
  @Output() deleteDistributorEvent = new EventEmitter();
  emailControl: FormControl;
  phoneControl: FormControl;
  innControl: FormControl;
  typeControl: FormControl;
  classificationControl: FormControl;


  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {

  }

  ngOnChanges(changes) {
    if (changes.currentItem &&
      changes.currentItem.previousValue !==
      changes.currentItem.currentValue &&
      changes.currentItem.currentValue
    ) {
      this.emailControl = this.fb.control(this.currentItem.email,
        Validators.compose([
          Validators.email
        ]));
      this.phoneControl = this.fb.control(this.currentItem.phone,
        Validators.compose([
          Validators.pattern(phoneRegex)
        ]));
      this.innControl = this.fb.control(this.currentItem.inn,
        Validators.compose([
          Validators.pattern(innRegex)
        ]));
      this.typeControl = this.fb.control(this.currentItem.clientTypeString);
    }
    if (changes.innChangeError) {
      // console.log(changes.innChangeError);
    }
  }

  getFields() {
    return Object.keys(this.itemFields);
  }

  getLeftButtonTitle() {
    if (this.cardMode === CardMode.VIEW) {
      return 'Редактировать';
    } else {
      return 'Сохранить';
    }
  }

  getRightButtonTitle() {
    if (this.cardMode === CardMode.VIEW) {
      return 'Закрыть';
    } else {
      return 'Отмена';
    }
  }

  getInnError() {
    // console.log('err', this.innChangeError);
    return this.innChangeError !== null;
  }

  isReadonly(field: string) {
    if (this.cardMode === CardMode.VIEW) {
      return true;
    } else {
      if (this.cardMode === CardMode.CREATE) {
        if (field === 'inn') {
          return false;
        }
        return (!this.itemFields[field].editable);
      } else {
        // CardMode.EDIT
        return (!this.itemFields[field].editable);
      }
    }
  }

  onLeftBtnClick() {
    this.leftBtnEvent.emit(this.currentItem);
  }

  onRightBtnClick() {
    this.rightBtnEvent.emit();
  }

  onSalesVolumeChange(event) {
    this.currentItem.salesVolume = event;
  }

  shouldShowDate() {
    return this.cardMode === CardMode.VIEW;
  }

  formatDate(value) {
    let datePipe = new DatePipe('en');
    return datePipe.transform(value, 'dd.MM.yyyy');
  }

  isViewMode() {
    return this.cardMode === CardMode.VIEW;
  }

  isRequiredMissing() {
    for (let field in this.itemFields) {
      if (this.itemFields[field].required && !this.currentItem[field]) {
        return true;
      }
    }
    return false;
  }

  isSaveDisabled() {
    if (this.cardMode !== CardMode.VIEW) {
      return this.isRequiredMissing() ||
        this.innControl.invalid ||
        this.emailControl.invalid ||
        this.phoneControl.invalid;
    }
  }

  isNoValInput(field: string) {
    return field !== 'email' &&
      field !== 'phone' &&
      field !== 'inn' &&
      field !== 'clientTypeString';
  }

  onChange(field) {
    // console.log(field);
    // console.log(this.currentItem[field]);
    if (field === 'email') {
      console.log(this.emailControl.value);
      this.currentItem.email = this.emailControl.value;
    }
    if (field === 'phone') {
      this.currentItem.phone = this.phoneControl.value;
    }
    if (field === 'inn') {
      if (this.innControl.valid) {
        this.innChangeEvent.emit(this.innControl.value);
      } else {
        this.innChangeEvent.emit(null);
      }
    }
  }

  shouldShowClientDistributors() {
    return this.currentItem.salesVolume || this.cardMode === CardMode.CREATE;
  }
}
