import { Injectable } from '@angular/core';
import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot, Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { Role, paths, pathAccess } from '../assets/constants';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private auth: AuthService, private route: Router) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.auth.isLoggedIn) {
      const role = this.auth.userData.roleName as Role;
      if (state.url === paths.login) {
        this.route.navigate([paths.clients]);
        return false;
      } else {
        let url = state.url;
        if (!Object.values(paths).includes(url)) {
          let path = state.url.split('/');
          path = path.splice(0, path.length - 1);
          url = path.join('/');
        }
        if (pathAccess[url].includes(role)) {
          return true;
        } else {
          this.route.navigate([paths.clients]);
          return false;
        }
      }
    } else {
      if (state.url !== paths.login) {
        this.route.navigate([paths.login]);
        return false;
      }
    }
  }
}
