import { FormControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { DataService, DistributorShortData } from './../data.service';
import { CardMode, paths } from './../../assets/constants';
import { MatTableDataSource } from '@angular/material';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-client-distributors-table',
  templateUrl: './client-distributors-table.component.html',
  styleUrls: ['./client-distributors-table.component.css']
})
export class ClientDistributorsTableComponent implements OnInit {
  @Input() data;
  @Input() cardMode;
  @Output() dataChangedEvent = new EventEmitter();
  displayedColumns = [
    "nameDistributor", "volumeFirstYear", "volumeSecondYear"
  ];
  tableData: MatTableDataSource<any>;
  isAddingNew: boolean = false;
  distributorsList: Object | null = null;
  distributorForm: FormGroup;
  distributorControl: FormControl;
  volumeFirstControl: FormControl;
  volumeSecondControl: FormControl;
  

  constructor(
    private dataService: DataService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.tableData = new MatTableDataSource([...this.data, this.getTotalVolume()]);
    this.getDistributors();
    this.distributorControl = this.fb.control(null, Validators.required);
    this.volumeFirstControl = this.fb.control(0, Validators.min(0));
    this.volumeSecondControl = this.fb.control(0, Validators.min(0));  
    this.distributorForm = this.fb.group({
      distributor: this.distributorControl,
      valueFirst: this.volumeFirstControl,
      valueSecond: this.volumeSecondControl,
    });  
  }

  ngOnChanges(changes) {
    if (changes.cardMode) {
      if (this.cardMode === CardMode.VIEW) {
        this.displayedColumns = [
          "nameDistributor", "volumeFirstYear", "volumeSecondYear"
        ];
      } else {
        this.displayedColumns = [
          "nameDistributor", "volumeFirstYear", "volumeSecondYear", "deleteItem"
        ];
      }
    }
    if (changes.data) {
      this.tableData = new MatTableDataSource(this.data);
    }
  }

  getTotalVolume() {
    let firstSum = 0;
    let secondSum = 0;
    for (let item of this.data) {
      firstSum += +item.volumeFirstYear;
      secondSum += +item.volumeSecondYear;
    }
    return {
      nameDistributor: 'Итого',
      volumeFirstYear: firstSum,
      volumeSecondYear: secondSum,
    }
  }

  onItemDelete(element) {
    this.data = this.data.filter(item => item.idDistributor !== element.idDistributor);
    this.dataChangedEvent.emit(this.data);
    this.tableData = new MatTableDataSource([...this.data, this.getTotalVolume()]);

  }

  getOptionName = value => {
    if (value) {
      return value.name;
    } else {
      return '';
    } 
  }

  onAddClick() {
    if (this.isAddingNew) {
      this.isAddingNew = false;
      const item = {
        idDistributor: this.distributorControl.value.id,
        nameDistributor: this.distributorControl.value.name,
        volumeFirstYear: this.volumeFirstControl.value,
        volumeSecondYear: this.volumeSecondControl.value,
      };
      this.data.push(item);
      this.dataChangedEvent.emit(this.data);
      this.tableData = new MatTableDataSource([...this.data, this.getTotalVolume()]);
      this.distributorControl.reset();
      this.volumeFirstControl.reset(0);
      this.volumeSecondControl.reset(0);
    } else {
      this.isAddingNew = true;
    }
  }

  isNotLastRow(element) {
    return element.nameDistributor !== 'Итого';
  }

  onDiscardClick() {
    this.isAddingNew = false;
  }

  getDistributors() {
    this.dataService.getDistributorList().subscribe(
      res => {
        this.distributorsList = res.map(item => ({
          id: item.id, name: item.name
        }));
      },
      err => {
        console.log(err);
        this.distributorsList = null;
      }
    )
  }

  getLink(element) {
    return paths.distributors + `/${element.idDistributor}`;
  }

  isViewMode() {
    return this.cardMode === CardMode.VIEW;
  }

}
