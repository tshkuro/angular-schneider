import { MatTableDataSource } from '@angular/material';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-table',
  templateUrl: './card-table.component.html',
  styleUrls: ['./card-table.component.css']
})
export class CardTableComponent implements OnInit {
  @Input() data;
  displayedColumns: string[] | null = null;
  tableData: MatTableDataSource<any>;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes) {
    if (changes.data) {
      this.tableData = new MatTableDataSource(this.data.data);
      this.displayedColumns = Object.keys(this.data.columnsOptions);
    }
  }
}
