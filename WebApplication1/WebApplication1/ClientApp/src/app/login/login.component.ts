import { Component } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-login-component',
  templateUrl: './login.component.html',
  styleUrls: ['../app.component.css', './login.component.css']
})
export class LoginComponent {
  userForm: FormGroup;
  emailControl: FormControl;
  passwordControl: FormControl;
  
  success = false;
  return: string = '';
  
  constructor(
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.emailControl = this.fb.control('', Validators.compose([
      Validators.required,
      Validators.email
    ]));
    this.passwordControl = this.fb.control('', Validators.compose([
      Validators.required,
      Validators.minLength(3)
    ]));
    this.userForm = this.fb.group({
      email: this.emailControl,
      password: this.passwordControl,
    });
  }

  get isLoggedIn() {
    return this.auth.isLoggedIn;
  }

  
  loginUser(event) {
    event.preventDefault();
    
    this.auth.login(this.userForm.value.email, this.userForm.value.password)
    .subscribe(
      res => {
        let token = res.access_token;
        this.auth.setToken(token);
        this.route.queryParams.subscribe(params => {
          this.return = params['return'] || '/clients';
          this.go();
        });
        // setTimeout

      },
      err => {
        window.alert('Неверные логин или пароль');
      }
    )
    
    // console.log('login', this.auth.token);

    // if (this.success) {
    //   this.route.queryParams.subscribe(params => {
    //     this.return = params['return'] || '/clients';
    //     if (this.auth.isLoggedIn) {
    //       this.go();
    //     }
    //   });
    // }
  }

  private go() {
    this.router.navigateByUrl(this.return);
  }

  logoutUser() {
    this.auth.logout();
  }

}
