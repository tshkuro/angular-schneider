import { ALL } from './../../assets/constants';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements OnInit {
  @Input() data;
  @Input() displayedColumns;
  @Input() filterValue;
  @Input() columnsOptions;
  @Input() filters;
  @Input() cardOpened;
  listData: MatTableDataSource<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  currentItemId: number;
  @Output() public rowClickEvent = new EventEmitter();

  constructor() { }

  ngOnInit() {
    if (this.data) {
      this.listData = new MatTableDataSource(this.data);
      this.listData.sort = this.sort;
      this.listData.paginator = this.paginator;
      this.listData.filterPredicate = (data, filter) => {
        let result = true;
        for (let column of this.displayedColumns) {
          const type = this.columnsOptions[column].type;

          if (this.filters[column]) {
            if (type === 'text') {
              result = result && (data[column].toLowerCase()
                .includes(this.filters[column].toLowerCase()));
            } else if (type === 'select') {
              result = result && (this.filters[column] === ALL ||
                data[column] === this.filters[column]);
            } else if (type === 'date') {
              const date = new Date(data[column]).setHours(0, 0, 0, 0);
              const filterDate = this.filters[column].setHours(0, 0, 0, 0);

              result = result && (date === filterDate);
            }
          }
        }
        return result;
      }
    }
  }

  ngOnChanges(changes) {
    if (changes.filterValue) {
      if (this.listData) {
        this.listData.filter = this.filterValue;
      }
    }
  }

  isDate(column) {
    return column === 'date';
  }

  onRowClick(row) {
    this.currentItemId = row.id;
    this.rowClickEvent.emit(row);
  }
}
