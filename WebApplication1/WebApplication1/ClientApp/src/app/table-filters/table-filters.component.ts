import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { DateAdapter } from '@angular/material/core';

@Component({
  selector: 'app-table-filters',
  templateUrl: './table-filters.component.html',
  styleUrls: ['./table-filters.component.css'],
})
export class TableFiltersComponent implements OnInit {
  @Input() displayedColumns;
  @Input() columnsOptions;
  @Input() filters;
  @Output() filterEvent = new EventEmitter();

  constructor(private adapter: DateAdapter<any>) { }

  ngOnInit() {
    this.adapter.setLocale('ru');
  }
  applyFilter(column: string, value: string | number | Date) {
    this.filterEvent.emit({column, value});
  }

  onClear(column) {
    this.filterEvent.emit({column, value: null});
  }

}