import { CardMode, cardWidth } from './../../assets/constants';
import { ClassificationData, DataService } from './../data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-classificationsPage',
  templateUrl: './classificationsPage.component.html',
  styleUrls: ['./classificationsPage.component.css']
})
export class ClassificationsPageComponent implements OnInit {
  smallScreenWidth = false;
  data: ClassificationData[] | null = null;

  displayedColumns: string[] = [
    'name', 'date',
  ];
  columnsOptions = {
    'name': { type: 'text', title: 'Классификация' },
    'date': { type: 'date', title: 'Дата создания' },
  };
  filters = {
    'name': null,
    'date': null,
  }
  filterValue = '';

  cardOpened = false;
  cardMode: CardMode = CardMode.VIEW;
  currentItem: ClassificationData | null = null;
  itemFields = {
    'name': { type: 'text', editable: false, title: 'Название', required: true },
    'date': { type: 'date', editable: false, title: 'Дата создания', required: false },
    'active': { type: 'checkbox', editable: true, title: 'Отключить', required: false},
  };
  constructor(
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.dataService.getClassifications().subscribe(
      res => {
        console.log(res);
        this.data = res;
      },
      err => {
        console.log(err);
        this.data = null;
      });
  }

  onRowClick(event) {
    console.log(event)
    this.cardOpened = true;
    this.getData(event.name);
    this.cardMode = CardMode.VIEW;
  }

  newItem() {
    this.cardOpened = true;
    this.currentItem = {};
    this.cardMode = CardMode.CREATE;
  }

  closeCard() {
    this.cardOpened = false;
    this.currentItem = null;
    this.cardMode = CardMode.VIEW;
  }

  enableEditMode() {
    this.cardMode = CardMode.EDIT;
  }

  onLeftButtonClick(itemData) {
    if (this.cardMode === CardMode.VIEW) {
      this.cardMode = CardMode.EDIT;
    } else {
      if (this.cardMode === CardMode.CREATE) {
        this.dataService.saveNewClient(this.currentItem);
      } else {
        this.dataService.saveClient(this.currentItem);
      }
      // save, if success => VIEW
      this.cardMode = CardMode.VIEW;
    }
  }

  onRightButtonClick() {
    if (this.cardMode === CardMode.EDIT) {
      this.cardMode = CardMode.VIEW;
      // discard
      this.getData(this.currentItem.name);
    } else {
      // VIEW OR CREATE
      this.closeCard();
    }
  }

  getData(name: string) {
    this.dataService.getClassificationByName(name).subscribe(
      res => {
        this.currentItem = res;
      },
      err => {
        console.log(err);
        this.currentItem = null;
      }
    )
  }

  changeFilters(event) {
    let { column, value } = event;
    if (value === '') {
      value = null;
    }
    this.filters[column] = value;
    this.filterValue = this.filters[column];
  }

  getFilterValue() {
    return this.filters;
  }

  getCardStyle() {
    let style = {
      // 'height': '90vh',
      'position': 'absolute',
      'top': '10px',
      'bottom': '10px',
      'right': '10px',
      'width': `${this.cardOpened ? cardWidth : 0}px`,
      'background-color': 'white',
    }
    if (this.smallScreenWidth) {
      style['left'] = '10px';
      style['width'] = `calc(100% - 20px)`;
    }
    return style;
  }

  getStyle() {
    return {
      'width': `calc(100% - ${this.cardOpened ? cardWidth + 10 : 0}px)`,
    }
  }

  shouldShowTable() {
    return !(this.cardOpened && this.smallScreenWidth);
  }

  getButtonsStyle() {
    if (this.smallScreenWidth) {
      return {
        'position': 'absolute',
        'right': '10px',
        'bottom': '10px',
      };
    } else {
      return {
        'position': 'absolute',
        'right': '10px',
        'top': '10px',
      };
    }
  }

}
