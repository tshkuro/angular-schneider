import { cardWidth, navWidth, smallNavWidth, SMALL_SCREEN_SIZE } from './../assets/constants';
import { AuthService } from './auth.service';
import { Component, HostListener } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  mode = new FormControl('side');
  sidenavSmall = false;
  cardOpened = false;
  smallScreenWidth: boolean = false;

  constructor(
    private auth: AuthService,
  ) {

  }

  ngOnInit() {
    this.smallScreenWidth = window.innerWidth < SMALL_SCREEN_SIZE;
    this.sidenavSmall = this.smallScreenWidth;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.smallScreenWidth = event.target.innerWidth < SMALL_SCREEN_SIZE;
  }

  toggleNav() {
    this.sidenavSmall = !this.sidenavSmall;
  }

  getContentStyle() {
    let width = 0;
    let left = 0;
    if (this.sidenavSmall) {
      width += smallNavWidth;
      left = smallNavWidth;
    } else {
      width += navWidth;
      left = navWidth;
    }
    if (this.cardOpened) {
      width += cardWidth;
    }
    return {
      'background-color': 'rgb(231, 231, 231)',
      'height': '100vh',
      'width': `calc(100% - ${width}px)`,
      'position': 'absolute',
      'left': `${left}px`,
    };
  }

  getSidenavStyle() {
    let style = {
      'position': 'absolute',
      'top': '0',
      'bottom': '0',
    };
    if (this.sidenavSmall) {
      style['width'] = `${smallNavWidth}px`;
    }
    return style;
  }

  getSidenavContentStyle() {
    return {
      'display': 'flex',
      'justify-content': 'center',
      'width': `${this.sidenavSmall ? smallNavWidth : navWidth }px`,
      'overflow': 'hidden',
    }
  }

  getCardStyle() {
    return {
      'height': '100vh',
      'position': 'absolute',
      'top': '0',
      'right': '0',
      'width': `${this.cardOpened ? cardWidth : 0 }px`
    }
  }
}
