import { DistributorData } from './data.service';
import { URL, BRONZE, SILVER, GOLD } from './../assets/constants';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { a } from '@angular/core/src/render3';

export interface ClientShortData {
  id: number;
  name: string;
  inn: string;
  date: number;
  clientTypeString?: string;
  clientClassificationString?: string;
}

export interface SalesVolumeData {
  idDistributor: number,
  nameDistributor: string,
  volumeFirstYear: number,
  volumeSecondYear: number  
}

export interface ClientData {
  id?: number,
  inn?: string,
  name?: string,
  email?: string,
  address?: string,
  clientClassificationString?: string,
  clientTypeString?: string,
  date?: number,
  phone?: string,
  salesVolume?: SalesVolumeData[],
  site?: string,
}

export interface DistributorShortData {
  id: number,
  name: string,
  date: number,
  inn: string,
  contactPerson: string,
  email: string,
  mobilePhone: string,
  phone: string
  address: string,
}

export interface DistributorData {
  address?: string,
  contactPerson?: string,
  date?: number,
  description?: string,
  email?: string,
  id?: number,
  inn?: string,
  mobilePhone?: string,
  name?: string,
  phone?: string
}

export interface ClassificationData {
  id?: number;
  name?: string;
  date?: number;
  active?: boolean;
}

export interface CollectionData {
  name?: string;
  active?: boolean;
  bronze?: number;
  silver?: number;
  gold?: number;
  date?: number;
}

export interface UserData {
  id?: number;
  username?: string;
  fullName?: string;
  isActive?: boolean;
  distributorId?: number;
  distributorName?: number;
  roleName?: string;
  createdDate?: number;
  lastModifiedDate?: number;
}

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private http: HttpClient, private auth: AuthService) {
    
  }

  getClientList() {
    const url = URL + '/client/list?view=short&page=1&size=20';
    const options = { headers: this.auth.authHeader()};
    return this.http.get<ClientShortData[]>(url, options);
  }

  getClientById(id: number) {
    const url = URL + `/client/${id}`;
    const options = { headers: this.auth.authHeader()};
    return this.http.get<ClientData>(url, options);
  }

  saveClient(clientData: ClientData) {
    console.log('pretend to save changes for client');
    console.log(clientData);
  }

  saveNewClient(clientData: ClientData) {
    console.log('pretend to save new client');
    console.log(clientData);
  }  

  getClassifications(active = true) {
    const url = URL + `/classification/list?active=${active}`;
    const options = { headers: this.auth.authHeader()};
    return this.http.get<ClassificationData[]>(url, options);
  }

  getClassificationByName(name: string) {
    const url = URL + `/classification/${name}`;
    const options = { headers: this.auth.authHeader()};
    return this.http.get<ClassificationData>(url, options);
  }

  getCollections(active = true) {
    const url = URL + `/collection/list?active=${active}`;
    const options = { headers: this.auth.authHeader()};
    return this.http.get<CollectionData[]>(url, options);  
  }

  getTypes() {
    return [BRONZE, SILVER, GOLD];
  }

  searchByInn(inn: string) {
    const url = URL + `/fns/search/client/${inn}`;
    const options = { headers: this.auth.authHeader()};
    return this.http.get(url, options);
  }

  searchClientByInn(inn: string) {
    const url = URL + `/client/searchbyinn/${inn}`;
    const options = { headers: this.auth.authHeader()};
    return this.http.get(url, options);  
  }

  searchDistributorByInn(inn: string) {
    const url = URL + `/distributor/searchbyinn/${inn}`;
    const options = { headers: this.auth.authHeader()};
    return this.http.get(url, options);  
  }

  getDistributorList() {
    const url = URL + '/distributor/list';
    const options = { headers: this.auth.authHeader()};
    return this.http.get<DistributorShortData[]>(url, options);
  }

  getDistributorById(id: number) {
    const url = URL + `/distributor/${id}`;
    const options = { headers: this.auth.authHeader()};
    return this.http.get<DistributorData>(url, options);
  }

  getDistributorUsers(id: number) {
    const url = URL + `/users/search/distributor/${id}`;
    const options = { headers: this.auth.authHeader()};
    return this.http.get<UserData[]>(url, options);
  }

  saveDistributor(distributorData: DistributorData) {
    console.log('pretend to save changes for distributor');
    console.log(distributorData);
  }

  saveNewDistributor(distributorData: DistributorData) {
    console.log('pretend to save new distributor');
    console.log(distributorData);
  }  
}
