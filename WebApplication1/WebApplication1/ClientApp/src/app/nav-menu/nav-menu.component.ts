import { pathAccess, Role } from './../../assets/constants';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  @Input() sidenavSmall: boolean; 
  @Output() public toggleEvent = new EventEmitter();

  constructor(
    private auth: AuthService,
    private router: Router,
  ) { }

  appTitle: string = 'My app';

  toggleNav() {
    this.toggleEvent.emit('');
  }

  isActive(link: string) {
    return this.router.url.includes(link);
  }

  get isLoggedIn() {
    return this.auth.isLoggedIn;
  }

  checkAccess(path: string) {
    return pathAccess[path].includes(this.auth.userData.roleName as Role);
  }


}
