import { AppDateAdapter } from './date-adapter';
import { MaterialModule } from './material/material.module';
import { TabsComponent } from './tabs/tabs.component';
import { TabComponent } from './tabs/tab.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Router, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';
import { DataService } from './data.service';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { MyHomeComponent } from './home1/home.component';
import { LoginComponent } from './login/login.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClientsPageComponent } from './clientsPage/clientsPage.component';
import { DataTableComponent } from './data-table/data-table.component';
import { TableFiltersComponent } from './table-filters/table-filters.component';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { CardComponent } from './card/card.component';
import { DistributorsPageComponent } from './distributorsPage/distributorsPage.component';
import { ClientDistributorsTableComponent } from './client-distributors-table/client-distributors-table.component';
import { TasksPageComponent } from './tasksPage/tasksPage.component';
import { ClassificationsPageComponent } from './classificationsPage/classificationsPage.component';
import { CollectionsPageComponent } from './collectionsPage/collectionsPage.component';
import { UsersPageComponent } from './usersPage/usersPage.component';
import { LoadingComponent } from './loading/loading.component';
import { CardTableComponent } from './card-table/card-table.component';

const routes: Routes = [
   { path: '', redirectTo: '/login', pathMatch: 'full'},
   { path: 'login', component: LoginComponent, canActivate: [AuthGuard] },
   { path: 'home', component: MyHomeComponent, canActivate: [AuthGuard] },
   { path: 'clients', component: ClientsPageComponent, canActivate: [AuthGuard] },  
   { path: 'distributors', component: DistributorsPageComponent, canActivate: [AuthGuard] },
   { path: 'distributors/:id', component: DistributorsPageComponent, canActivate: [AuthGuard] },
   { path: 'tasks', component: TasksPageComponent, canActivate: [AuthGuard] },
   { path: 'classifications', component: ClassificationsPageComponent, canActivate: [AuthGuard] },
   { path: 'collections', component: CollectionsPageComponent, canActivate: [AuthGuard] },
   { path: 'users', component: UsersPageComponent, canActivate: [AuthGuard] },
];

@NgModule({
   declarations: [
      AppComponent,
      NavMenuComponent,
      MyHomeComponent,
      LoginComponent,
      TabComponent,
      TabsComponent,
      NotfoundComponent,
      ClientsPageComponent,
      DataTableComponent,
      TableFiltersComponent,
      CardComponent,
      DistributorsPageComponent,
      ClientDistributorsTableComponent,
      TasksPageComponent,
      ClassificationsPageComponent,
      CollectionsPageComponent,
      UsersPageComponent,
      LoadingComponent,
      CardTableComponent
   ],
   imports: [
      BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
      HttpClientModule,
      FormsModule,
      MaterialModule,
      ReactiveFormsModule,
      RouterModule.forRoot(routes),
      BrowserAnimationsModule,
   ],
   providers: [
      AuthService,
      AuthGuard,
      DataService,
      { provide: DateAdapter, useClass: AppDateAdapter },
   ],
   bootstrap: [AppComponent],
})
export class AppModule { }
