export const URL = "http://132.145.235.80:8011";

export const BRONZE = 'Бронзовый';
export const SILVER = 'Серебряный';
export const GOLD = 'Золотой';
export const ALL = 'Все';

export const translateType = {
    [BRONZE]: 'bronze',
    [SILVER]: 'silver',
    [GOLD]: 'gold',
    'bronze': BRONZE,
    'silver': SILVER,
    'gold': GOLD,   
}

export const navWidth = 170;
export const smallNavWidth = 50;
export const cardWidth = 300;

export const SMALL_SCREEN_SIZE = 960;

export enum CardMode {
    VIEW, EDIT, CREATE
};

export enum Role {
    ADMIN = 'ROLE_ADMIN', USER = 'ROLE_DISTRIBUTOR'
};

export const translateRole = {
    'ROLE_ADMIN': 'Администратор',
    'ROLE_DISTRIBUTOR': 'Дистрибьютор',
    'Администратор': 'ROLE_ADMIN',
    'Дистрибьютор': 'ROLE_DISTRIBUTOR',
}

export const paths = {
    login: '/login',
    clients: '/clients',
    tasks: '/tasks',
    distributors: '/distributors',
    classifications: '/classifications',
    collections: '/collections',
    users: '/users'
};

export const pathAccess = {
    [paths.login]: [Role.ADMIN, Role.USER],
    [paths.clients]: [Role.ADMIN, Role.USER],
    [paths.tasks]: [Role.ADMIN, Role.USER],
    [paths.distributors]: [Role.ADMIN],
    [paths.classifications]: [Role.ADMIN],
    [paths.collections]: [Role.ADMIN],
    [paths.users]: [Role.ADMIN],
  };

export const phoneRegex = /(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s.]?[(]?[0-9]{1,3}[)]?([-\s.]?[0-9]{3})([-\s.]?[0-9]{2}[-.]?[0-9]{2})/;
export const innRegex = /[\d+]{10,12}/;